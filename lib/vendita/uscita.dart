import 'package:flutter/material.dart';
import 'package:ristorazione/auth/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/services.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ristorazione/home_page.dart';




class uscita extends StatefulWidget {
  var email;

  final String name;
 
  final quantita;
  final document;
 


  uscita({this.auth, this.onSignOut, Key id, this.email,this.name,this.quantita,this.document});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


   @override
  _uscitaState createState() => new _uscitaState(); 

}

 class _uscitaState extends State<uscita>{

 BaseAuth auth;
 VoidCallback onSignOut;
 var document;
 
 String name='';
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

  String misura='pz';
  int _radioValue = 0;
  String quantita='0';
  String quantitavenduta='0';
  String note;


   Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
 


@override
  void initState() {
    // TODO: implement initState
    auth=widget.auth;
    onSignOut=widget.onSignOut;

    name=widget.name;
 
    quantita=widget.quantita;
    document=widget.document;


    //blocco data
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);

  }

  
    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "kg";
          break;
        case 2:
          misura = "lt";
          break;
      }
    });
  }

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }

    void cassa(){

        double quantitatr;
        
        quantitatr=double.parse(quantita);
        
        double quantitatrvenduta;
        quantitatrvenduta=double.parse(quantitavenduta);

       print(quantitatr);
       print(quantitatrvenduta);
       
       num totale;
       totale=quantitatr-quantitatrvenduta;
       print(totale);

       if(totale<0){

           showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Attenzione il valore supera la quantità del magazzino.'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Torna dietro',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              
            ],
          )
        );


       }
       else{
       _addData(totale);
       }

      




    }


     void _addData(nuovostato){
     double quantitatr;
     quantitatr=double.parse(quantitavenduta);
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('vendita');
       await reference.add({
         "nome":name,
         "data":_testodata,
         "misura":misura,
         "quantita":quantitatr,
         "note":note
         
       });      
     });

     Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(document);
      await transaction.update(snapshot.reference, {
        "quantita": nuovostato,
      });
    });

    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new HomePage(),
                        ),
                        ModalRoute.withName('/'));





    
     

  }

  void chiudi(){

  }



  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Uscita prodotto'),
        actions: <Widget>[
           IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {
                _signOut();
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,

      //bottone basso a destra

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.navigate_next),
       backgroundColor: Colors.brown,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Procedere con il salvataggio?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Cancella',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  
                  cassa();

                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
    


 
      //fine bottone


      body: Column(
        children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(

                  child: 
                  
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                   
                    
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Scheda informazioni vendita', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                    ),
                     Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data vendita:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),
          ),
                      Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Note:',style: TextStyle(fontSize: 18)),
                    ),
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: TextField(
                        
                        onChanged: (String str){
                          setState(() {
                            note=str;
                                  
                                            });
                          },
                          decoration: new InputDecoration(
                  
                  
                        
                  

                                ),
                              ),
                     ),
                         Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Quantità vendita (disponibile: $quantita):' ,style:new TextStyle(fontSize:18.0,color: Colors.black )),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: TextField(
                          keyboardType: TextInputType.number,
                          
                          onChanged: (String str){
                            setState(() {
                            quantitavenduta=str;        
                                            });
                          },
                          decoration: new InputDecoration(
                           
                            hintText: "",
                           

                            ),
                          ),
                       ),

                      
               

                    

               
            
                                  ],
                                ),
                ),
            ),


        ],
      )



    );




  }

    
 

}