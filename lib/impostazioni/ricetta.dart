import 'package:flutter/material.dart';
import 'package:ristorazione/auth/auth.dart';
import 'package:ristorazione/ingresso/final.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ristorazione/home_page.dart';



class ricetta extends StatefulWidget {
  var email;


  ricetta({this.auth, this.onSignOut, Key id, this.email});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


   @override
  _ricettaState createState() => new _ricettaState(); 

}

 class _ricettaState extends State<ricetta>{

 BaseAuth auth;
 VoidCallback onSignOut;
 
 String name='';
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

  String misura='pz';
  int _radioValue = 0;
  String quantita='';


   Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
 


@override
  void initState() {
    // TODO: implement initState
    auth=widget.auth;
    onSignOut=widget.onSignOut;

    //blocco data
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);

  }

  
    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "kg";
          break;
        case 2:
          misura = "lt";
          break;
      }
    });
  }

   void _addData(String ingrediente){
    
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('magazzino');
       await reference.add({
         "nome":name,
         "quantita":0
         

       });
      
       
     });

    
     

  }

   

  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Impostazioni'),
        actions: <Widget>[
           IconButton(
              icon: Icon(Icons.help),
              onPressed: () {
               
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,

      //bottone basso a destra

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.brown,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Aggiungere ingrediente?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Indietro',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Aggiungi',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  
                  _addData(name);
                   Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new HomePage(),
                        ),
                        ModalRoute.withName('/'));
                  
                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
    


 
      //fine bottone


      body: Column(
        children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(

                  child: 
                  
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                   
                    
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Record magazzino ', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                    ),
                    
                      Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Ingrediente',style: TextStyle(fontSize: 18)),
                    ),
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: TextField(
                        
                        onChanged: (String str){
                          setState(() {
                            name=str;
                                  
                                            });
                          },
                          decoration: new InputDecoration(
                  
                  
                        
                  

                                ),
                              ),
                     ),
                         
                     
            
                                  ],
                                ),
                ),
            ),


        ],
      )



    );




  }

    
 

}