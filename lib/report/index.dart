import 'package:flutter/material.dart';
import 'package:ristorazione/auth/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ristorazione/funzioni/crud.dart';


class report extends StatefulWidget {
  var email;

  final String name;
  final data;
  final quantita;
  final misura;


  report({this.auth, this.onSignOut, Key id, this.email,this.name,this.data,this.quantita,this.misura});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


   @override
  _reportState createState() => new _reportState(); 

}




 class _reportState extends State<report>{

 BaseAuth auth;
 VoidCallback onSignOut;
 
 String name='';
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

  String misura='pz';
  int _radioValue = 0;
  String quantita='';


   Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
 


@override
  void initState() {
    // TODO: implement initState
    auth=widget.auth;
    onSignOut=widget.onSignOut;

    name=widget.name;
    _testodata=widget.data;
    quantita=widget.quantita;
    misura=widget.misura;

    //blocco data
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);

  }

  
    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "kg";
          break;
        case 2:
          misura = "lt";
          break;
      }
    });
  }

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }

  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Ingressi registrati oggi'),
        actions: <Widget>[
           IconButton(
              icon: Icon(Icons.help),
              onPressed: () {
              
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,

      //bottone basso a destra

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.search),
       backgroundColor: Colors.brown,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Procedere con la ricerca?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Indietro',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  
                  
                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
    


 
      //fine bottone


      body: 
      
       new Stack(
     
     children: 
     
     <Widget>[ 
          
     
     new Padding(
            padding: const EdgeInsets.only(top:40),
            child:
     
     StreamBuilder(

       stream: Firestore.instance
       .collection("ingredienti").where("data", isEqualTo: _testodata)
       .snapshots(),

      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
           return new Container( child: Center(
           child: CircularProgressIndicator()
        ),

        );
      
      
      
      
      
      
      return new dettaglivendite(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )
      
      
 ///fine vecchio



    );




  }

    
 

}



class dettaglivendite extends StatefulWidget{

    final List<DocumentSnapshot> document;
    dettaglivendite({this.document});
    @override
    _ListavenditeState createState() => new _ListavenditeState();
  
}

class _ListavenditeState extends State<dettaglivendite>{
 
 Crud crud = new Crud();  //richiamo i miei strumenti


 
 
 List<DocumentSnapshot> document;

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      
    }

      


 @override
   Widget build(BuildContext context){
     String etichetta='';
     String datalavorazione='';

     return new Scaffold(
       //inizio scaffold


       body:   
       

       
       Container(
         color: Colors.redAccent,
         child: ListView.builder(
              itemCount: document.length,
            
              itemBuilder: (BuildContext context, int i) {
              String nome=document[i].data['nome'].toString();
              String quantita=document[i].data['quantita'].toString();   
              String dataproduzione=document[i].data['data'].toString();
              String misura=document[i].data['misura'].toString();
              String fornitore=document[i].data['fornitore'].toString();
            
            
    return new Padding(padding: new EdgeInsets.all(10.0),
          child: new Card(
            child: new Column(
              children: <Widget>[
                new ListTile(
                  title: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom:12.0),
                        child: Row(children: <Widget>[Text(nome, style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold))],),
                      ),
                      Row(
                        children: <Widget>[

                            Text('Quantità:', style: TextStyle(fontSize: 18),),
                            Text(quantita,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),
                            Text(' $misura',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),
                         
                
                        ],
                      ),
                    ],
                  ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top:12.0),
                  child: Text('Registrato il $dataproduzione, fornito da $fornitore'),
                ),
                ),
                
                

                    new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                       new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.edit, color: Colors.brown,
                        
                        ),
                        Text('Modifica',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        

                      },
                    ),
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.delete, color: Colors.red,
                        
                        ),
                        Text('Elimina',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                            showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Confermi eliminazione? '),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Indietro',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){

                 
               crud.elimina(document[i].reference).then((result) {
               Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new report(),
                        ),
                        ModalRoute.withName('/'));
              });

              


          
                  
                  
                  
                  
                },
              )
            ],
          )
        );

                        

                       


                      

                      },
                    ),
                    
                  ],
                ),
              ),
                
              
              ],
            ),
          )
    ); 
            }),
       ),

       //finescaffol

     );
 
   }   _reportState createState() => new _reportState();
}









