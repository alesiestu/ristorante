import 'package:flutter/material.dart';
import 'package:ristorazione/auth/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/services.dart';


class mostra extends StatefulWidget {
  var email;

  final String name;
  final data;
  final quantita;
  final misura;


  mostra({this.auth, this.onSignOut, Key id, this.email,this.name,this.data,this.quantita,this.misura});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


   @override
  _mostraState createState() => new _mostraState(); 

}

 class _mostraState extends State<mostra>{

 BaseAuth auth;
 VoidCallback onSignOut;
 
 String name='';
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

  String misura='pz';
  int _radioValue = 0;
  String quantita='';


   Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
 


@override
  void initState() {
    // TODO: implement initState
    auth=widget.auth;
    onSignOut=widget.onSignOut;

    name=widget.name;
    _testodata=widget.data;
    quantita=widget.quantita;
    misura=widget.misura;

    //blocco data
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);

  }

  
    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "kg";
          break;
        case 2:
          misura = "lt";
          break;
      }
    });
  }

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }

  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Ingresso'),
        actions: <Widget>[
           IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {
                _signOut();
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,

      //bottone basso a destra

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.navigate_next),
       backgroundColor: Colors.brown,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Procedere con il salvataggio?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Cancella',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  
                  
                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
    


 
      //fine bottone


      body: Column(
        children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(

                  child: 
                  
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                   
                    
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Scheda informazioni prodotto', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                    ),
                     Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data ingresso:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),
          ),
                      Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Nome:',style: TextStyle(fontSize: 18)),
                    ),
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: TextField(
                        
                        onChanged: (String str){
                          setState(() {
                            name=str;
                                  
                                            });
                          },
                          decoration: new InputDecoration(
                  
                  
                        
                  

                                ),
                              ),
                     ),
                         Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Quantità prodotta' ,style:new TextStyle(fontSize:18.0,color: Colors.black )),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: TextField(
                          keyboardType: TextInputType.number,
                          
                          onChanged: (String str){
                            setState(() {
                            quantita=str;        
                                            });
                          },
                          decoration: new InputDecoration(
                           
                            hintText: "",
                           

                            ),
                          ),
                       ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Radio(
                              value: 0,
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text('Pz'),
                            new Radio(
                              value: 1,
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text('Kg'),
                            new Radio(
                              value: 2,
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text('Lt'),
                          ],
                        ),
               

                    

               
            
                                  ],
                                ),
                ),
            ),


        ],
      )



    );




  }

    
 

}