import 'package:flutter/material.dart';
import 'package:ristorazione/auth/auth.dart';
import 'package:ristorazione/ingresso/final.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';



class venditareport extends StatefulWidget {
  var email;


  venditareport({this.auth, this.onSignOut, Key id, this.email});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


   @override
  _venditareportState createState() => new _venditareportState(); 

}

 class _venditareportState extends State<venditareport>{

 BaseAuth auth;
 VoidCallback onSignOut;
 
 String name='';
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

  String misura='pz';
  int _radioValue = 0;
  String quantita='0';


   Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
 


@override
  void initState() {
    // TODO: implement initState
    auth=widget.auth;
    onSignOut=widget.onSignOut;

    //blocco data
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);

  }

  
    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "kg";
          break;
        case 2:
          misura = "lt";
          break;
      }
    });
  }

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }

    void controllo(String name){

    final String _collection = 'magazzino';
    final Firestore _fireStore = Firestore.instance;

    if(quantita==''){
      setState(() {
        quantita='0';
      });
    }
    
    num stato;
    double nuovostato;
    double quantitatr;
    quantitatr=double.parse(quantita);
    num totale;
    var document;
    

    
    getData() async {
    return await _fireStore.collection(_collection).where("nome", isEqualTo: name).getDocuments();
    }

    getData().then((val){
      if(val.documents.length > 0){
        setState(() {
          stato=val.documents[0].data["quantita"];
          document=val.documents[0].reference;
         
        });
        print(stato);
        print(quantitatr);
        totale=stato+quantitatr;
        print(totale);

        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new chiusura(
                         name: name,
                         data: _testodata,
                         quantita: quantita,
                         misura: misura,
                         nuovostato: totale,
                         document:document
                         
                          
                          )
                        ));

        
      }
      else{
        print('Non trovato');
          showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Ingrediente non trovato, aggiungere nuovo ingrediente?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Indietro',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){

                  Firestore.instance.runTransaction((Transaction transsaction) async{
                  CollectionReference reference= Firestore.instance.collection('magazzino');
                  await reference.add({
                    "nome":name,
                    "quantita":0
                    

                  });

                  controllo(name);
      
       
               });
                  
                  
                  
                  
                },
              )
            ],
          )
        );

      }
    
    });

    


    
 


    }

  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Ricerca'),
        actions: <Widget>[
           IconButton(
              icon: Icon(Icons.help),
              onPressed: () {
               
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,

      //bottone basso a destra

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.navigate_next),
       backgroundColor: Colors.brown,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Salvare e continuare?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Indietro',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Avanti',style: TextStyle(fontSize: 14)),
                onPressed: (){

                  controllo(name);
                  
                  
                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
    


 
      //fine bottone


      body: Column(
        children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(

                  child: 
                  
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                   
                    
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Visualizza vendite', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                    ),
                     Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data vendita:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),
          ),
                      Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Nome:',style: TextStyle(fontSize: 18)),
                    ),
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: TextField(
                        
                        onChanged: (String str){
                          setState(() {
                            name=str;
                                  
                                            });
                          },
                          decoration: new InputDecoration(
                  
                  
                        
                  

                                ),
                              ),
                     ),
                         Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Quantità' ,style:new TextStyle(fontSize:18.0,color: Colors.black )),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: TextField(
                          keyboardType: TextInputType.number,
                          
                          onChanged: (String str){
                            setState(() {
                            quantita=str;        
                                            });
                          },
                          decoration: new InputDecoration(
                           
                            hintText: "",
                           

                            ),
                          ),
                       ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Radio(
                              value: 0,
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text('Pz'),
                            new Radio(
                              value: 1,
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text('Kg'),
                            new Radio(
                              value: 2,
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text('Lt'),
                          ],
                        ),
               

                    

               
            
                                  ],
                                ),
                ),
            ),


        ],
      )



    );




  }

    
 

}