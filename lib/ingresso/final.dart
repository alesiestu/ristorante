import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:ristorazione/auth/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter/services.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ristorazione/home_page.dart';



class chiusura extends StatefulWidget {
  var email;
 final document;

  final String name;
  final data;
  final quantita;
  final misura;
  final double nuovostato;


  chiusura({this.auth, this.onSignOut, Key id, this.email,this.name,this.data,this.quantita,this.misura,this.nuovostato,this.document});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


   @override
  _chiusuraState createState() => new _chiusuraState(); 

}

 class _chiusuraState extends State<chiusura>{

 BaseAuth auth;
 VoidCallback onSignOut;

 double nuovostato;
 
 String name='';
 String fornitore='';
 DateTime _datacorrente= new DateTime.now();
 String _testodata = '';

  String misura='pz';
  int _radioValue = 0;
  String quantita='';

  String prezzo='';
  String ricarico='';
  var document;


   Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

   void _addData(){
     double quantitatr;
     quantitatr=double.parse(quantita);
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('ingredienti');
       await reference.add({
         "nome":name,
         "data":_testodata,
         "misura":misura,
         "quantita":quantitatr,
         "fornitore":fornitore,
         "prezzoacquisto":prezzo,
         "ricarico":ricarico,
       });      
     });

     Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(document);
      await transaction.update(snapshot.reference, {
        "quantita": nuovostato,
      });
    });

    
     

  }
 


@override
  void initState() {
    // TODO: implement initState
    auth=widget.auth;
    onSignOut=widget.onSignOut;

    name=widget.name;
    _testodata=widget.data;
    quantita=widget.quantita;
    misura=widget.misura;
    //da passare al magazzino
    nuovostato=widget.nuovostato;
    document=widget.document;

    //blocco data
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);

  }

  
    void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "kg";
          break;
        case 2:
          misura = "lt";
          break;
      }
    });
  }

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }

  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Fornitura'),
        actions: <Widget>[
           IconButton(
              icon: Icon(Icons.help),
              onPressed: () {
                
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,

      //bottone basso a destra

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.brown,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Procedere con il salvataggio?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Cancella',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  
                  _addData();
                  Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new HomePage(),
                        ),
                        ModalRoute.withName('/'));
                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
    


 
      //fine bottone


      body: Column(
        children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(

                  child: 
                  
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                   
                    
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Scheda fornitura', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                    ),
                 
                      Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Fornitore:',style: TextStyle(fontSize: 18)),
                    ),
                     Padding(
                       padding: const EdgeInsets.all(8.0),
                       child: TextField(
                        
                        onChanged: (String str){
                          setState(() {
                            fornitore=str;
                                  
                                            });
                          },
                          decoration: new InputDecoration(
                  
                  
                        
                  

                                ),
                              ),
                     ),
                         Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Prezzo di acquisto' ,style:new TextStyle(fontSize:18.0,color: Colors.black )),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: TextField(
                          keyboardType: TextInputType.number,
                          
                          onChanged: (String str){
                            setState(() {
                            prezzo=str;        
                                            });
                          },
                          decoration: new InputDecoration(
                           
                            hintText: "",
                           

                            ),
                          ),
                       ),
                             Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Ricarico' ,style:new TextStyle(fontSize:18.0,color: Colors.black )),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: TextField(
                          keyboardType: TextInputType.number,
                          
                          onChanged: (String str){
                            setState(() {
                            ricarico=str;        
                                            });
                          },
                          decoration: new InputDecoration(
                           
                            hintText: "",
                           

                            ),
                          ),
                       ),

                   

                    

               
            
                                  ],
                                ),
                ),
            ),


        ],
      )



    );




  }

    
 

}