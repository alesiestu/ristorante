import 'package:flutter/material.dart';
import 'auth/auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:ristorazione/ingresso/registra.dart';
import 'package:ristorazione/report/index.dart';
import 'package:ristorazione/report/menu.dart';
import 'package:ristorazione/impostazioni/ricetta.dart';
import 'package:ristorazione/vendita/index.dart';


class HomePage extends StatelessWidget {
  var email;

  HomePage({this.auth, this.onSignOut, Key id, this.email});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 


  
  String _email() {
    if (currentUser != null) {
      return currentUser.displayName;
    } else {
      return "no current user";
    }
  }


    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }

  @override
  Widget build(BuildContext context) {

     return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.brown,
        title: new Text('Pannello amministrativo'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new ricetta()),
                    );
              },
            ),
           IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {
                _signOut();
              },
            ),
        ],
      ),
      backgroundColor: Colors.redAccent,
      body: Padding(
        padding: const EdgeInsets.only(top:18.0),
        child: StaggeredGridView.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 12.0,
                  mainAxisSpacing: 12.0,
                  padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                  children: <Widget>[
                    _buildTile(
                     
                      Padding
                      (
                        padding: const EdgeInsets.all(24.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            Column
                            (
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>
                              [
                                
                                Text('Materie prime', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                              ],
                            ),
                            Material
                            (
                              
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center
                              (
                                child: Padding
                                (
                                   padding: const EdgeInsets.all(4.0),
                                  child: new Image.asset('assets/image/camion.png')
                               //   child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                               
                                )
                              )
                            )
                          ]
                        ),
                      ),
                      onTap: (){
                         Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new ingresso()),
                    );
                        }
                    ),
                     _buildTile(
                     
                      Padding
                      (
                        padding: const EdgeInsets.all(24.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            Column
                            (
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>
                              [
                                
                                Text('Vendita', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                              ],
                            ),
                            Material
                            (
                              
                           //   color: Colors.red,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center
                              (
                                child: Padding
                                (
                                  padding: const EdgeInsets.all(4.0),
                                  child: new Image.asset('assets/image/cassa.png')
                                  //child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                                  
                                )
                              )
                            )
                          ]
                        ),
                      ),
                      onTap: (){

                            Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new vendita()),
                    );
                      
                      }
                      
                    ),

                      _buildTile(
                     
                      Padding
                      (
                        padding: const EdgeInsets.all(24.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            Column
                            (
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>
                              [
                                
                                Text('Report', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                              ],
                            ),
                            Material
                            (
                              
                           //   color: Colors.red,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center
                              (
                                child: Padding
                                (
                                  padding: const EdgeInsets.all(4.0),
                                  child: new Image.asset('assets/image/report.png')
                                  //child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                                  
                                )
                              )
                            )
                          ]
                        ),
                      ),
                      onTap: (){
                         Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new menureport()),
                    );
                      
                      }
                      
                    ),
                 
                    
                   
                  ],
                  staggeredTiles: [
                    StaggeredTile.extent(2, 110.0),
                    StaggeredTile.extent(2, 110.0),
                    StaggeredTile.extent(2, 110.0),
                    
                
                    
                  ],
                ),
      )
    );




  }

    Widget _buildTile(Widget child, {Function() onTap}) {
            return Material(
              elevation: 14.0,
              borderRadius: BorderRadius.circular(12.0),
              shadowColor: Color(0x802196F3),
              child: InkWell
              (
                // Do onTap() if it isn't null, otherwise do print()
                onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
                child: child
              )
            );
          }
 

}