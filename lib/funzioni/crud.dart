import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ristorazione/auth/auth.dart';


class Crud {

   
   
  elimina(var document) async {
    print('elimina');
    Firestore.instance.runTransaction((transaction)async{
                                DocumentSnapshot snapshot=
                                await transaction.get(document);
                                await transaction.delete(snapshot.reference);
                              });
    
  }

   getAll(String collection) async 
    {
        return await Firestore.instance.collection(collection).snapshots();
    }
  

}
